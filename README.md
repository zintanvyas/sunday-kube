step-1

master: 2 cpu's and 6GB RAM
worker: 1 cpu   &   3GB RAM

OS: RHEL7/CentOS7

Step:2 Disable selinux, swap, firewall

firewall:
systemctl disable firewalld
systemctl stop  firewalld

swap:
swapoff -a 

selinux:
setenforce 0
sed -i 's/enforcing/disabled/g' /etc/selinux/config
grep disabled /etc/selinux/config | grep -v '#' 


step-3  Install docker 

yum update -y
yum install -y docker 

systemctl enable docker
systemctl start docker
systemctl status docker 


step-4 Install kubeadm kubelet kubectl

Add kubernetes Repo:

cat <<EOF> /etc/yum.repos.d/kubernetes.repos
[kubernetes]
name=kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kube*
EOF 

yum install -y kubeadm kubelet kubectl --disableexcludes=kubernetes
systemctl enable kubelet
systemctl start kubelet
systemctl status kubelet


cat <<EOF > /etc/sysctl.d/k8s.conf 
net.bridge.bridge-nf-call-ip6tables = 1 
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system


Step:5  Initialize master node ( only on master )

kubeadm init --pod-network-cidr=<cidrblock>


from the o/p of the above command copy and run those commands
mkdir.....
sudo ....
sudo chown ....

join token....


step:6

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/a70459be0084506e4ec919aa1c114638878db11b/Documentation/kube-flannel.yml

kubectl get pods --all-namespaces



step:7 ( on worker )


kubeadm join command......







